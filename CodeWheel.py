#Author-Dag Rede
#Description-go between two diameters accrding to string of 0 and 1s

import adsk.core, adsk.fusion, adsk.cam, traceback, math, os

def log(msg):
    try:
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'log.txt'), 'a+') as file:
            file.write(msg + "\n")
    except:
        pass

def run(context):
    ui = None
    try:
        innerRadius = 1.0
        outerRadius = 1.3

        app = adsk.core.Application.get()
        design = app.activeProduct
        timeLine = design.timeline
        timeLineGroups = timeLine.timelineGroups
        ui  = app.userInterface

        sels = ui.activeSelections
        if sels.count > 0:
            face = sels.item(0).entity
            if type(face).__name__ == "BRepFace" and face.geometry.surfaceType == adsk.core.SurfaceTypes.PlaneSurfaceType:                
                component = face.body.parentComponent
                sketches = component.sketches

                timelineCurrentIndex = timeLine.markerPosition

                # digits = "1000111011"
                digits = "0111000100"
                # digits = "0000000000"
                # digits = "1111111111"
                # digits = "0101010101"
                # digits = "1000000000"
                # digits = "0111111111"
                digitCount = len(digits)
                digitAngle = 2.0 * math.pi / digitCount

                sketch = sketches.add(face)
                # sketch.name = "code curve"
                sketchArcs = sketch.sketchCurves.sketchArcs
                sketchCircles = sketch.sketchCurves.sketchCircles

                r = innerRadius
                t = outerRadius - innerRadius
                v = digitAngle
                # calculate u = radius of arc for 101
                a = r*math.sin(v)
                b = r*(1-math.cos(v))
                u = (a*a+b*b-t*t)/(2*(t-b))
                w = math.asin(a/(u + t))
                vw = math.pi - v - w
                ru = r + u
                vAroundOuter = w
                vAroundInner = math.pi - vw

                wheelPoint = lambda i, r, angle: adsk.core.Point3D.create(
                        r * math.cos(i * angle), 
                        r * math.sin(i * angle), 0)


                nextArcStart = wheelPoint(0, innerRadius if digits[0] == '0' else outerRadius, digitAngle)

                for i in range(digitCount):
                    iNext = (i + 1) % digitCount
                    dig2 = "{}{}".format(digits[i], digits[iNext])  # this and next digit

                    # draw one of 4 arcs according to case
                    if dig2 == '00' or dig2 == '11':
                        arcCenter = adsk.core.Point3D.create(0, 0, 0)
                        latestArc = sketchArcs.addByCenterStartSweep(arcCenter, nextArcStart, digitAngle)   
                        nextArcStart = latestArc.endSketchPoint
                    elif dig2 == '01':
                        arcCenter = wheelPoint(i, ru, digitAngle)
                        # arcStart = wheelPoint(i, innerRadius, digitAngle)
                        latestArc = sketchArcs.addByCenterStartSweep(arcCenter, nextArcStart, -vAroundOuter)
                        nextArcStart = latestArc.startSketchPoint

                        arcCenter = wheelPoint(iNext, innerRadius, digitAngle)
                        # arcStart = wheelPoint(iNext, outerRadius, digitAngle)
                        latestArc = sketchArcs.addByCenterStartSweep(arcCenter, nextArcStart, vAroundInner)
                        nextArcStart = latestArc.endSketchPoint
                    else: # dig2 == '10'
                        arcCenter = wheelPoint(i, innerRadius, digitAngle)
                        # arcStart = wheelPoint(i, outerRadius, digitAngle)
                        latestArc = sketchArcs.addByCenterStartSweep(arcCenter, nextArcStart, vAroundInner)
                        nextArcStart = latestArc.endSketchPoint

                        arcCenter = wheelPoint(iNext, ru, digitAngle)
                        # arcStart = wheelPoint(iNext, innerRadius, digitAngle)
                        latestArc = sketchArcs.addByCenterStartSweep(arcCenter, nextArcStart, -vAroundOuter)
                        nextArcStart = latestArc.startSketchPoint

                timelineEndIndex = timeLine.markerPosition
                if timelineEndIndex - timelineCurrentIndex > 1:
                    exportTimelineGroup = timeLineGroups.add(timelineCurrentIndex, timelineEndIndex-1)# the minus 1 thing works, weird.
                    exportTimelineGroup.name = 'code wheel group'


    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))
